
module fir_tb;
    parameter clkperiod=2;
    reg clk;
    reg signed [31:0] in;
    wire signed [63:0] out;
	
 fir fir_test(.clk(clk),.in(in),.out(out));
    
initial clk = 0;
always #(clkperiod/2) clk =~clk;

initial 
	begin
          in = 0;  #(3*clkperiod);
          in = -3; #clkperiod;
          in = 1;  #clkperiod;
          in = 0;  #clkperiod;
          in = -2; #clkperiod;
          in = -1; #clkperiod;
          in = 4;  #clkperiod;
          in = -5; #clkperiod;
          in = 6;  #clkperiod;
          in = 0;  #clkperiod;
    end
      
endmodule