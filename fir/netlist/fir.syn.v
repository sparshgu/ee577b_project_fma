/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : K-2015.06-SP5-5
// Date      : Thu Nov  8 23:50:16 2018
/////////////////////////////////////////////////////////////


module fir_DW01_sub_1 ( A, B, CI, DIFF, CO );
  input [63:0] A;
  input [63:0] B;
  output [63:0] DIFF;
  input CI;
  output CO;
  wire   n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35;
  wire   [63:2] carry;

  FAX1 U2_63 ( .A(A[63]), .B(n4), .C(carry[63]), .YC(), .YS(DIFF[63]) );
  FAX1 U2_62 ( .A(A[62]), .B(n4), .C(carry[62]), .YC(carry[63]), .YS(DIFF[62])
         );
  FAX1 U2_61 ( .A(A[61]), .B(n4), .C(carry[61]), .YC(carry[62]), .YS(DIFF[61])
         );
  FAX1 U2_60 ( .A(A[60]), .B(n3), .C(carry[60]), .YC(carry[61]), .YS(DIFF[60])
         );
  FAX1 U2_59 ( .A(A[59]), .B(n3), .C(carry[59]), .YC(carry[60]), .YS(DIFF[59])
         );
  FAX1 U2_58 ( .A(A[58]), .B(n3), .C(carry[58]), .YC(carry[59]), .YS(DIFF[58])
         );
  FAX1 U2_57 ( .A(A[57]), .B(n3), .C(carry[57]), .YC(carry[58]), .YS(DIFF[57])
         );
  FAX1 U2_56 ( .A(A[56]), .B(n3), .C(carry[56]), .YC(carry[57]), .YS(DIFF[56])
         );
  FAX1 U2_55 ( .A(A[55]), .B(n3), .C(carry[55]), .YC(carry[56]), .YS(DIFF[55])
         );
  FAX1 U2_54 ( .A(A[54]), .B(n3), .C(carry[54]), .YC(carry[55]), .YS(DIFF[54])
         );
  FAX1 U2_53 ( .A(A[53]), .B(n3), .C(carry[53]), .YC(carry[54]), .YS(DIFF[53])
         );
  FAX1 U2_52 ( .A(A[52]), .B(n3), .C(carry[52]), .YC(carry[53]), .YS(DIFF[52])
         );
  FAX1 U2_51 ( .A(A[51]), .B(n3), .C(carry[51]), .YC(carry[52]), .YS(DIFF[51])
         );
  FAX1 U2_50 ( .A(A[50]), .B(n3), .C(carry[50]), .YC(carry[51]), .YS(DIFF[50])
         );
  FAX1 U2_49 ( .A(A[49]), .B(n3), .C(carry[49]), .YC(carry[50]), .YS(DIFF[49])
         );
  FAX1 U2_48 ( .A(A[48]), .B(n3), .C(carry[48]), .YC(carry[49]), .YS(DIFF[48])
         );
  FAX1 U2_47 ( .A(A[47]), .B(n3), .C(carry[47]), .YC(carry[48]), .YS(DIFF[47])
         );
  FAX1 U2_46 ( .A(A[46]), .B(n3), .C(carry[46]), .YC(carry[47]), .YS(DIFF[46])
         );
  FAX1 U2_45 ( .A(A[45]), .B(n3), .C(carry[45]), .YC(carry[46]), .YS(DIFF[45])
         );
  FAX1 U2_44 ( .A(A[44]), .B(n3), .C(carry[44]), .YC(carry[45]), .YS(DIFF[44])
         );
  FAX1 U2_43 ( .A(A[43]), .B(n3), .C(carry[43]), .YC(carry[44]), .YS(DIFF[43])
         );
  FAX1 U2_42 ( .A(A[42]), .B(n3), .C(carry[42]), .YC(carry[43]), .YS(DIFF[42])
         );
  FAX1 U2_41 ( .A(A[41]), .B(n3), .C(carry[41]), .YC(carry[42]), .YS(DIFF[41])
         );
  FAX1 U2_40 ( .A(A[40]), .B(n3), .C(carry[40]), .YC(carry[41]), .YS(DIFF[40])
         );
  FAX1 U2_39 ( .A(A[39]), .B(n3), .C(carry[39]), .YC(carry[40]), .YS(DIFF[39])
         );
  FAX1 U2_38 ( .A(A[38]), .B(n3), .C(carry[38]), .YC(carry[39]), .YS(DIFF[38])
         );
  FAX1 U2_37 ( .A(A[37]), .B(n4), .C(carry[37]), .YC(carry[38]), .YS(DIFF[37])
         );
  FAX1 U2_36 ( .A(A[36]), .B(n4), .C(carry[36]), .YC(carry[37]), .YS(DIFF[36])
         );
  FAX1 U2_35 ( .A(A[35]), .B(n4), .C(carry[35]), .YC(carry[36]), .YS(DIFF[35])
         );
  FAX1 U2_34 ( .A(A[34]), .B(n4), .C(carry[34]), .YC(carry[35]), .YS(DIFF[34])
         );
  FAX1 U2_33 ( .A(A[33]), .B(n4), .C(carry[33]), .YC(carry[34]), .YS(DIFF[33])
         );
  FAX1 U2_32 ( .A(A[32]), .B(n4), .C(carry[32]), .YC(carry[33]), .YS(DIFF[32])
         );
  FAX1 U2_31 ( .A(A[31]), .B(n4), .C(carry[31]), .YC(carry[32]), .YS(DIFF[31])
         );
  FAX1 U2_30 ( .A(A[30]), .B(n5), .C(carry[30]), .YC(carry[31]), .YS(DIFF[30])
         );
  FAX1 U2_29 ( .A(A[29]), .B(n6), .C(carry[29]), .YC(carry[30]), .YS(DIFF[29])
         );
  FAX1 U2_28 ( .A(A[28]), .B(n7), .C(carry[28]), .YC(carry[29]), .YS(DIFF[28])
         );
  FAX1 U2_27 ( .A(A[27]), .B(n8), .C(carry[27]), .YC(carry[28]), .YS(DIFF[27])
         );
  FAX1 U2_26 ( .A(A[26]), .B(n9), .C(carry[26]), .YC(carry[27]), .YS(DIFF[26])
         );
  FAX1 U2_25 ( .A(A[25]), .B(n10), .C(carry[25]), .YC(carry[26]), .YS(DIFF[25]) );
  FAX1 U2_24 ( .A(A[24]), .B(n11), .C(carry[24]), .YC(carry[25]), .YS(DIFF[24]) );
  FAX1 U2_23 ( .A(A[23]), .B(n12), .C(carry[23]), .YC(carry[24]), .YS(DIFF[23]) );
  FAX1 U2_22 ( .A(A[22]), .B(n13), .C(carry[22]), .YC(carry[23]), .YS(DIFF[22]) );
  FAX1 U2_21 ( .A(A[21]), .B(n14), .C(carry[21]), .YC(carry[22]), .YS(DIFF[21]) );
  FAX1 U2_20 ( .A(A[20]), .B(n15), .C(carry[20]), .YC(carry[21]), .YS(DIFF[20]) );
  FAX1 U2_19 ( .A(A[19]), .B(n16), .C(carry[19]), .YC(carry[20]), .YS(DIFF[19]) );
  FAX1 U2_18 ( .A(A[18]), .B(n17), .C(carry[18]), .YC(carry[19]), .YS(DIFF[18]) );
  FAX1 U2_17 ( .A(A[17]), .B(n18), .C(carry[17]), .YC(carry[18]), .YS(DIFF[17]) );
  FAX1 U2_16 ( .A(A[16]), .B(n19), .C(carry[16]), .YC(carry[17]), .YS(DIFF[16]) );
  FAX1 U2_15 ( .A(A[15]), .B(n20), .C(carry[15]), .YC(carry[16]), .YS(DIFF[15]) );
  FAX1 U2_14 ( .A(A[14]), .B(n21), .C(carry[14]), .YC(carry[15]), .YS(DIFF[14]) );
  FAX1 U2_13 ( .A(A[13]), .B(n22), .C(carry[13]), .YC(carry[14]), .YS(DIFF[13]) );
  FAX1 U2_12 ( .A(A[12]), .B(n23), .C(carry[12]), .YC(carry[13]), .YS(DIFF[12]) );
  FAX1 U2_11 ( .A(A[11]), .B(n24), .C(carry[11]), .YC(carry[12]), .YS(DIFF[11]) );
  FAX1 U2_10 ( .A(A[10]), .B(n25), .C(carry[10]), .YC(carry[11]), .YS(DIFF[10]) );
  FAX1 U2_9 ( .A(A[9]), .B(n26), .C(carry[9]), .YC(carry[10]), .YS(DIFF[9]) );
  FAX1 U2_8 ( .A(A[8]), .B(n27), .C(carry[8]), .YC(carry[9]), .YS(DIFF[8]) );
  FAX1 U2_7 ( .A(A[7]), .B(n28), .C(carry[7]), .YC(carry[8]), .YS(DIFF[7]) );
  FAX1 U2_6 ( .A(A[6]), .B(n29), .C(carry[6]), .YC(carry[7]), .YS(DIFF[6]) );
  FAX1 U2_5 ( .A(A[5]), .B(n30), .C(carry[5]), .YC(carry[6]), .YS(DIFF[5]) );
  FAX1 U2_4 ( .A(A[4]), .B(n31), .C(carry[4]), .YC(carry[5]), .YS(DIFF[4]) );
  FAX1 U2_3 ( .A(A[3]), .B(n32), .C(carry[3]), .YC(carry[4]), .YS(DIFF[3]) );
  FAX1 U2_2 ( .A(A[2]), .B(n33), .C(carry[2]), .YC(carry[3]), .YS(DIFF[2]) );
  FAX1 U2_1 ( .A(A[1]), .B(n34), .C(n2), .YC(carry[2]), .YS(DIFF[1]) );
  INVX1 U1 ( .A(B[1]), .Y(n34) );
  INVX1 U2 ( .A(B[31]), .Y(n4) );
  INVX1 U3 ( .A(B[63]), .Y(n3) );
  OR2X1 U4 ( .A(A[0]), .B(n35), .Y(n2) );
  INVX1 U5 ( .A(B[2]), .Y(n33) );
  INVX1 U6 ( .A(B[3]), .Y(n32) );
  INVX1 U7 ( .A(B[4]), .Y(n31) );
  INVX1 U8 ( .A(B[5]), .Y(n30) );
  INVX1 U9 ( .A(B[6]), .Y(n29) );
  INVX1 U10 ( .A(B[7]), .Y(n28) );
  INVX1 U11 ( .A(B[8]), .Y(n27) );
  INVX1 U12 ( .A(B[9]), .Y(n26) );
  INVX1 U13 ( .A(B[10]), .Y(n25) );
  INVX1 U14 ( .A(B[11]), .Y(n24) );
  INVX1 U15 ( .A(B[12]), .Y(n23) );
  INVX1 U16 ( .A(B[13]), .Y(n22) );
  INVX1 U17 ( .A(B[14]), .Y(n21) );
  INVX1 U18 ( .A(B[15]), .Y(n20) );
  INVX1 U19 ( .A(B[16]), .Y(n19) );
  INVX1 U20 ( .A(B[17]), .Y(n18) );
  INVX1 U21 ( .A(B[18]), .Y(n17) );
  INVX1 U22 ( .A(B[19]), .Y(n16) );
  INVX1 U23 ( .A(B[20]), .Y(n15) );
  INVX1 U24 ( .A(B[21]), .Y(n14) );
  INVX1 U25 ( .A(B[22]), .Y(n13) );
  INVX1 U26 ( .A(B[23]), .Y(n12) );
  INVX1 U27 ( .A(B[24]), .Y(n11) );
  INVX1 U28 ( .A(B[25]), .Y(n10) );
  INVX1 U29 ( .A(B[26]), .Y(n9) );
  INVX1 U30 ( .A(B[27]), .Y(n8) );
  INVX1 U31 ( .A(B[28]), .Y(n7) );
  INVX1 U32 ( .A(B[29]), .Y(n6) );
  INVX1 U33 ( .A(B[30]), .Y(n5) );
  INVX1 U34 ( .A(B[0]), .Y(n35) );
  XNOR2X1 U35 ( .A(n35), .B(A[0]), .Y(DIFF[0]) );
endmodule


module fir_DW01_sub_0 ( A, B, CI, DIFF, CO );
  input [63:0] A;
  input [63:0] B;
  output [63:0] DIFF;
  input CI;
  output CO;
  wire   n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35;
  wire   [63:3] carry;
  assign DIFF[0] = A[0];

  FAX1 U2_63 ( .A(A[63]), .B(n4), .C(carry[63]), .YC(), .YS(DIFF[63]) );
  FAX1 U2_62 ( .A(A[62]), .B(n4), .C(carry[62]), .YC(carry[63]), .YS(DIFF[62])
         );
  FAX1 U2_61 ( .A(A[61]), .B(n4), .C(carry[61]), .YC(carry[62]), .YS(DIFF[61])
         );
  FAX1 U2_60 ( .A(A[60]), .B(n3), .C(carry[60]), .YC(carry[61]), .YS(DIFF[60])
         );
  FAX1 U2_59 ( .A(A[59]), .B(n3), .C(carry[59]), .YC(carry[60]), .YS(DIFF[59])
         );
  FAX1 U2_58 ( .A(A[58]), .B(n3), .C(carry[58]), .YC(carry[59]), .YS(DIFF[58])
         );
  FAX1 U2_57 ( .A(A[57]), .B(n3), .C(carry[57]), .YC(carry[58]), .YS(DIFF[57])
         );
  FAX1 U2_56 ( .A(A[56]), .B(n3), .C(carry[56]), .YC(carry[57]), .YS(DIFF[56])
         );
  FAX1 U2_55 ( .A(A[55]), .B(n3), .C(carry[55]), .YC(carry[56]), .YS(DIFF[55])
         );
  FAX1 U2_54 ( .A(A[54]), .B(n3), .C(carry[54]), .YC(carry[55]), .YS(DIFF[54])
         );
  FAX1 U2_53 ( .A(A[53]), .B(n3), .C(carry[53]), .YC(carry[54]), .YS(DIFF[53])
         );
  FAX1 U2_52 ( .A(A[52]), .B(n3), .C(carry[52]), .YC(carry[53]), .YS(DIFF[52])
         );
  FAX1 U2_51 ( .A(A[51]), .B(n3), .C(carry[51]), .YC(carry[52]), .YS(DIFF[51])
         );
  FAX1 U2_50 ( .A(A[50]), .B(n3), .C(carry[50]), .YC(carry[51]), .YS(DIFF[50])
         );
  FAX1 U2_49 ( .A(A[49]), .B(n3), .C(carry[49]), .YC(carry[50]), .YS(DIFF[49])
         );
  FAX1 U2_48 ( .A(A[48]), .B(n3), .C(carry[48]), .YC(carry[49]), .YS(DIFF[48])
         );
  FAX1 U2_47 ( .A(A[47]), .B(n3), .C(carry[47]), .YC(carry[48]), .YS(DIFF[47])
         );
  FAX1 U2_46 ( .A(A[46]), .B(n3), .C(carry[46]), .YC(carry[47]), .YS(DIFF[46])
         );
  FAX1 U2_45 ( .A(A[45]), .B(n3), .C(carry[45]), .YC(carry[46]), .YS(DIFF[45])
         );
  FAX1 U2_44 ( .A(A[44]), .B(n3), .C(carry[44]), .YC(carry[45]), .YS(DIFF[44])
         );
  FAX1 U2_43 ( .A(A[43]), .B(n3), .C(carry[43]), .YC(carry[44]), .YS(DIFF[43])
         );
  FAX1 U2_42 ( .A(A[42]), .B(n3), .C(carry[42]), .YC(carry[43]), .YS(DIFF[42])
         );
  FAX1 U2_41 ( .A(A[41]), .B(n3), .C(carry[41]), .YC(carry[42]), .YS(DIFF[41])
         );
  FAX1 U2_40 ( .A(A[40]), .B(n3), .C(carry[40]), .YC(carry[41]), .YS(DIFF[40])
         );
  FAX1 U2_39 ( .A(A[39]), .B(n3), .C(carry[39]), .YC(carry[40]), .YS(DIFF[39])
         );
  FAX1 U2_38 ( .A(A[38]), .B(n3), .C(carry[38]), .YC(carry[39]), .YS(DIFF[38])
         );
  FAX1 U2_37 ( .A(A[37]), .B(n4), .C(carry[37]), .YC(carry[38]), .YS(DIFF[37])
         );
  FAX1 U2_36 ( .A(A[36]), .B(n4), .C(carry[36]), .YC(carry[37]), .YS(DIFF[36])
         );
  FAX1 U2_35 ( .A(A[35]), .B(n4), .C(carry[35]), .YC(carry[36]), .YS(DIFF[35])
         );
  FAX1 U2_34 ( .A(A[34]), .B(n4), .C(carry[34]), .YC(carry[35]), .YS(DIFF[34])
         );
  FAX1 U2_33 ( .A(A[33]), .B(n4), .C(carry[33]), .YC(carry[34]), .YS(DIFF[33])
         );
  FAX1 U2_32 ( .A(A[32]), .B(n4), .C(carry[32]), .YC(carry[33]), .YS(DIFF[32])
         );
  FAX1 U2_31 ( .A(A[31]), .B(n5), .C(carry[31]), .YC(carry[32]), .YS(DIFF[31])
         );
  FAX1 U2_30 ( .A(A[30]), .B(n6), .C(carry[30]), .YC(carry[31]), .YS(DIFF[30])
         );
  FAX1 U2_29 ( .A(A[29]), .B(n7), .C(carry[29]), .YC(carry[30]), .YS(DIFF[29])
         );
  FAX1 U2_28 ( .A(A[28]), .B(n8), .C(carry[28]), .YC(carry[29]), .YS(DIFF[28])
         );
  FAX1 U2_27 ( .A(A[27]), .B(n9), .C(carry[27]), .YC(carry[28]), .YS(DIFF[27])
         );
  FAX1 U2_26 ( .A(A[26]), .B(n10), .C(carry[26]), .YC(carry[27]), .YS(DIFF[26]) );
  FAX1 U2_25 ( .A(A[25]), .B(n11), .C(carry[25]), .YC(carry[26]), .YS(DIFF[25]) );
  FAX1 U2_24 ( .A(A[24]), .B(n12), .C(carry[24]), .YC(carry[25]), .YS(DIFF[24]) );
  FAX1 U2_23 ( .A(A[23]), .B(n13), .C(carry[23]), .YC(carry[24]), .YS(DIFF[23]) );
  FAX1 U2_22 ( .A(A[22]), .B(n14), .C(carry[22]), .YC(carry[23]), .YS(DIFF[22]) );
  FAX1 U2_21 ( .A(A[21]), .B(n15), .C(carry[21]), .YC(carry[22]), .YS(DIFF[21]) );
  FAX1 U2_20 ( .A(A[20]), .B(n16), .C(carry[20]), .YC(carry[21]), .YS(DIFF[20]) );
  FAX1 U2_19 ( .A(A[19]), .B(n17), .C(carry[19]), .YC(carry[20]), .YS(DIFF[19]) );
  FAX1 U2_18 ( .A(A[18]), .B(n18), .C(carry[18]), .YC(carry[19]), .YS(DIFF[18]) );
  FAX1 U2_17 ( .A(A[17]), .B(n19), .C(carry[17]), .YC(carry[18]), .YS(DIFF[17]) );
  FAX1 U2_16 ( .A(A[16]), .B(n20), .C(carry[16]), .YC(carry[17]), .YS(DIFF[16]) );
  FAX1 U2_15 ( .A(A[15]), .B(n21), .C(carry[15]), .YC(carry[16]), .YS(DIFF[15]) );
  FAX1 U2_14 ( .A(A[14]), .B(n22), .C(carry[14]), .YC(carry[15]), .YS(DIFF[14]) );
  FAX1 U2_13 ( .A(A[13]), .B(n23), .C(carry[13]), .YC(carry[14]), .YS(DIFF[13]) );
  FAX1 U2_12 ( .A(A[12]), .B(n24), .C(carry[12]), .YC(carry[13]), .YS(DIFF[12]) );
  FAX1 U2_11 ( .A(A[11]), .B(n25), .C(carry[11]), .YC(carry[12]), .YS(DIFF[11]) );
  FAX1 U2_10 ( .A(A[10]), .B(n26), .C(carry[10]), .YC(carry[11]), .YS(DIFF[10]) );
  FAX1 U2_9 ( .A(A[9]), .B(n27), .C(carry[9]), .YC(carry[10]), .YS(DIFF[9]) );
  FAX1 U2_8 ( .A(A[8]), .B(n28), .C(carry[8]), .YC(carry[9]), .YS(DIFF[8]) );
  FAX1 U2_7 ( .A(A[7]), .B(n29), .C(carry[7]), .YC(carry[8]), .YS(DIFF[7]) );
  FAX1 U2_6 ( .A(A[6]), .B(n30), .C(carry[6]), .YC(carry[7]), .YS(DIFF[6]) );
  FAX1 U2_5 ( .A(A[5]), .B(n31), .C(carry[5]), .YC(carry[6]), .YS(DIFF[5]) );
  FAX1 U2_4 ( .A(A[4]), .B(n32), .C(carry[4]), .YC(carry[5]), .YS(DIFF[4]) );
  FAX1 U2_3 ( .A(A[3]), .B(n33), .C(carry[3]), .YC(carry[4]), .YS(DIFF[3]) );
  FAX1 U2_2 ( .A(A[2]), .B(n34), .C(n2), .YC(carry[3]), .YS(DIFF[2]) );
  INVX1 U1 ( .A(B[2]), .Y(n34) );
  INVX1 U2 ( .A(B[32]), .Y(n4) );
  INVX1 U3 ( .A(B[63]), .Y(n3) );
  OR2X1 U4 ( .A(A[1]), .B(n35), .Y(n2) );
  INVX1 U5 ( .A(B[3]), .Y(n33) );
  INVX1 U6 ( .A(B[4]), .Y(n32) );
  INVX1 U7 ( .A(B[5]), .Y(n31) );
  INVX1 U8 ( .A(B[6]), .Y(n30) );
  INVX1 U9 ( .A(B[7]), .Y(n29) );
  INVX1 U10 ( .A(B[8]), .Y(n28) );
  INVX1 U11 ( .A(B[9]), .Y(n27) );
  INVX1 U12 ( .A(B[10]), .Y(n26) );
  INVX1 U13 ( .A(B[11]), .Y(n25) );
  INVX1 U14 ( .A(B[12]), .Y(n24) );
  INVX1 U15 ( .A(B[13]), .Y(n23) );
  INVX1 U16 ( .A(B[14]), .Y(n22) );
  INVX1 U17 ( .A(B[15]), .Y(n21) );
  INVX1 U18 ( .A(B[16]), .Y(n20) );
  INVX1 U19 ( .A(B[17]), .Y(n19) );
  INVX1 U20 ( .A(B[18]), .Y(n18) );
  INVX1 U21 ( .A(B[19]), .Y(n17) );
  INVX1 U22 ( .A(B[20]), .Y(n16) );
  INVX1 U23 ( .A(B[21]), .Y(n15) );
  INVX1 U24 ( .A(B[22]), .Y(n14) );
  INVX1 U25 ( .A(B[23]), .Y(n13) );
  INVX1 U26 ( .A(B[24]), .Y(n12) );
  INVX1 U27 ( .A(B[25]), .Y(n11) );
  INVX1 U28 ( .A(B[26]), .Y(n10) );
  INVX1 U29 ( .A(B[27]), .Y(n9) );
  INVX1 U30 ( .A(B[28]), .Y(n8) );
  INVX1 U31 ( .A(B[29]), .Y(n7) );
  INVX1 U32 ( .A(B[30]), .Y(n6) );
  INVX1 U33 ( .A(B[31]), .Y(n5) );
  INVX1 U34 ( .A(B[1]), .Y(n35) );
  XNOR2X1 U35 ( .A(n35), .B(A[1]), .Y(DIFF[1]) );
endmodule


module fir_DW_mult_tc_0 ( a, b, product );
  input [2:0] a;
  input [31:0] b;
  output [34:0] product;
  wire   n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17,
         n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31,
         n32;
  assign product[34] = b[31];
  assign product[33] = b[31];
  assign product[0] = b[0];

  FAX1 U3 ( .A(b[31]), .B(b[30]), .C(n3), .YC(product[32]), .YS(product[31])
         );
  FAX1 U4 ( .A(b[29]), .B(b[30]), .C(n4), .YC(n3), .YS(product[30]) );
  FAX1 U5 ( .A(b[28]), .B(b[29]), .C(n5), .YC(n4), .YS(product[29]) );
  FAX1 U6 ( .A(b[27]), .B(b[28]), .C(n6), .YC(n5), .YS(product[28]) );
  FAX1 U7 ( .A(b[26]), .B(b[27]), .C(n7), .YC(n6), .YS(product[27]) );
  FAX1 U8 ( .A(b[25]), .B(b[26]), .C(n8), .YC(n7), .YS(product[26]) );
  FAX1 U9 ( .A(b[24]), .B(b[25]), .C(n9), .YC(n8), .YS(product[25]) );
  FAX1 U10 ( .A(b[23]), .B(b[24]), .C(n10), .YC(n9), .YS(product[24]) );
  FAX1 U11 ( .A(b[22]), .B(b[23]), .C(n11), .YC(n10), .YS(product[23]) );
  FAX1 U12 ( .A(b[21]), .B(b[22]), .C(n12), .YC(n11), .YS(product[22]) );
  FAX1 U13 ( .A(b[20]), .B(b[21]), .C(n13), .YC(n12), .YS(product[21]) );
  FAX1 U14 ( .A(b[19]), .B(b[20]), .C(n14), .YC(n13), .YS(product[20]) );
  FAX1 U15 ( .A(b[18]), .B(b[19]), .C(n15), .YC(n14), .YS(product[19]) );
  FAX1 U16 ( .A(b[17]), .B(b[18]), .C(n16), .YC(n15), .YS(product[18]) );
  FAX1 U17 ( .A(b[16]), .B(b[17]), .C(n17), .YC(n16), .YS(product[17]) );
  FAX1 U18 ( .A(b[15]), .B(b[16]), .C(n18), .YC(n17), .YS(product[16]) );
  FAX1 U19 ( .A(b[14]), .B(b[15]), .C(n19), .YC(n18), .YS(product[15]) );
  FAX1 U20 ( .A(b[13]), .B(b[14]), .C(n20), .YC(n19), .YS(product[14]) );
  FAX1 U21 ( .A(b[12]), .B(b[13]), .C(n21), .YC(n20), .YS(product[13]) );
  FAX1 U22 ( .A(b[11]), .B(b[12]), .C(n22), .YC(n21), .YS(product[12]) );
  FAX1 U23 ( .A(b[10]), .B(b[11]), .C(n23), .YC(n22), .YS(product[11]) );
  FAX1 U24 ( .A(b[9]), .B(b[10]), .C(n24), .YC(n23), .YS(product[10]) );
  FAX1 U25 ( .A(b[8]), .B(b[9]), .C(n25), .YC(n24), .YS(product[9]) );
  FAX1 U26 ( .A(b[7]), .B(b[8]), .C(n26), .YC(n25), .YS(product[8]) );
  FAX1 U27 ( .A(b[6]), .B(b[7]), .C(n27), .YC(n26), .YS(product[7]) );
  FAX1 U28 ( .A(b[5]), .B(b[6]), .C(n28), .YC(n27), .YS(product[6]) );
  FAX1 U29 ( .A(b[4]), .B(b[5]), .C(n29), .YC(n28), .YS(product[5]) );
  FAX1 U30 ( .A(b[3]), .B(b[4]), .C(n30), .YC(n29), .YS(product[4]) );
  FAX1 U31 ( .A(b[2]), .B(b[3]), .C(n31), .YC(n30), .YS(product[3]) );
  FAX1 U32 ( .A(b[1]), .B(b[2]), .C(n32), .YC(n31), .YS(product[2]) );
  HAX1 U33 ( .A(b[0]), .B(b[1]), .YC(n32), .YS(product[1]) );
endmodule


module fir_DW01_add_2 ( A, B, CI, SUM, CO );
  input [63:0] A;
  input [63:0] B;
  output [63:0] SUM;
  input CI;
  output CO;
  wire   n1;
  wire   [63:4] carry;
  assign SUM[1] = B[1];
  assign SUM[0] = B[0];

  FAX1 U1_63 ( .A(A[63]), .B(B[63]), .C(carry[63]), .YC(), .YS(SUM[63]) );
  FAX1 U1_62 ( .A(A[62]), .B(B[62]), .C(carry[62]), .YC(carry[63]), .YS(
        SUM[62]) );
  FAX1 U1_61 ( .A(A[61]), .B(B[61]), .C(carry[61]), .YC(carry[62]), .YS(
        SUM[61]) );
  FAX1 U1_60 ( .A(A[60]), .B(B[60]), .C(carry[60]), .YC(carry[61]), .YS(
        SUM[60]) );
  FAX1 U1_59 ( .A(A[59]), .B(B[59]), .C(carry[59]), .YC(carry[60]), .YS(
        SUM[59]) );
  FAX1 U1_58 ( .A(A[58]), .B(B[58]), .C(carry[58]), .YC(carry[59]), .YS(
        SUM[58]) );
  FAX1 U1_57 ( .A(A[57]), .B(B[57]), .C(carry[57]), .YC(carry[58]), .YS(
        SUM[57]) );
  FAX1 U1_56 ( .A(A[56]), .B(B[56]), .C(carry[56]), .YC(carry[57]), .YS(
        SUM[56]) );
  FAX1 U1_55 ( .A(A[55]), .B(B[55]), .C(carry[55]), .YC(carry[56]), .YS(
        SUM[55]) );
  FAX1 U1_54 ( .A(A[54]), .B(B[54]), .C(carry[54]), .YC(carry[55]), .YS(
        SUM[54]) );
  FAX1 U1_53 ( .A(A[53]), .B(B[53]), .C(carry[53]), .YC(carry[54]), .YS(
        SUM[53]) );
  FAX1 U1_52 ( .A(A[52]), .B(B[52]), .C(carry[52]), .YC(carry[53]), .YS(
        SUM[52]) );
  FAX1 U1_51 ( .A(A[51]), .B(B[51]), .C(carry[51]), .YC(carry[52]), .YS(
        SUM[51]) );
  FAX1 U1_50 ( .A(A[50]), .B(B[50]), .C(carry[50]), .YC(carry[51]), .YS(
        SUM[50]) );
  FAX1 U1_49 ( .A(A[49]), .B(B[49]), .C(carry[49]), .YC(carry[50]), .YS(
        SUM[49]) );
  FAX1 U1_48 ( .A(A[48]), .B(B[48]), .C(carry[48]), .YC(carry[49]), .YS(
        SUM[48]) );
  FAX1 U1_47 ( .A(A[47]), .B(B[47]), .C(carry[47]), .YC(carry[48]), .YS(
        SUM[47]) );
  FAX1 U1_46 ( .A(A[46]), .B(B[46]), .C(carry[46]), .YC(carry[47]), .YS(
        SUM[46]) );
  FAX1 U1_45 ( .A(A[45]), .B(B[45]), .C(carry[45]), .YC(carry[46]), .YS(
        SUM[45]) );
  FAX1 U1_44 ( .A(A[44]), .B(B[44]), .C(carry[44]), .YC(carry[45]), .YS(
        SUM[44]) );
  FAX1 U1_43 ( .A(A[43]), .B(B[43]), .C(carry[43]), .YC(carry[44]), .YS(
        SUM[43]) );
  FAX1 U1_42 ( .A(A[42]), .B(B[42]), .C(carry[42]), .YC(carry[43]), .YS(
        SUM[42]) );
  FAX1 U1_41 ( .A(A[41]), .B(B[41]), .C(carry[41]), .YC(carry[42]), .YS(
        SUM[41]) );
  FAX1 U1_40 ( .A(A[40]), .B(B[40]), .C(carry[40]), .YC(carry[41]), .YS(
        SUM[40]) );
  FAX1 U1_39 ( .A(A[39]), .B(B[39]), .C(carry[39]), .YC(carry[40]), .YS(
        SUM[39]) );
  FAX1 U1_38 ( .A(A[38]), .B(B[38]), .C(carry[38]), .YC(carry[39]), .YS(
        SUM[38]) );
  FAX1 U1_37 ( .A(A[37]), .B(B[37]), .C(carry[37]), .YC(carry[38]), .YS(
        SUM[37]) );
  FAX1 U1_36 ( .A(A[36]), .B(B[36]), .C(carry[36]), .YC(carry[37]), .YS(
        SUM[36]) );
  FAX1 U1_35 ( .A(A[35]), .B(B[35]), .C(carry[35]), .YC(carry[36]), .YS(
        SUM[35]) );
  FAX1 U1_34 ( .A(A[34]), .B(B[34]), .C(carry[34]), .YC(carry[35]), .YS(
        SUM[34]) );
  FAX1 U1_33 ( .A(A[33]), .B(B[33]), .C(carry[33]), .YC(carry[34]), .YS(
        SUM[33]) );
  FAX1 U1_32 ( .A(A[32]), .B(B[32]), .C(carry[32]), .YC(carry[33]), .YS(
        SUM[32]) );
  FAX1 U1_31 ( .A(A[31]), .B(B[31]), .C(carry[31]), .YC(carry[32]), .YS(
        SUM[31]) );
  FAX1 U1_30 ( .A(A[30]), .B(B[30]), .C(carry[30]), .YC(carry[31]), .YS(
        SUM[30]) );
  FAX1 U1_29 ( .A(A[29]), .B(B[29]), .C(carry[29]), .YC(carry[30]), .YS(
        SUM[29]) );
  FAX1 U1_28 ( .A(A[28]), .B(B[28]), .C(carry[28]), .YC(carry[29]), .YS(
        SUM[28]) );
  FAX1 U1_27 ( .A(A[27]), .B(B[27]), .C(carry[27]), .YC(carry[28]), .YS(
        SUM[27]) );
  FAX1 U1_26 ( .A(A[26]), .B(B[26]), .C(carry[26]), .YC(carry[27]), .YS(
        SUM[26]) );
  FAX1 U1_25 ( .A(A[25]), .B(B[25]), .C(carry[25]), .YC(carry[26]), .YS(
        SUM[25]) );
  FAX1 U1_24 ( .A(A[24]), .B(B[24]), .C(carry[24]), .YC(carry[25]), .YS(
        SUM[24]) );
  FAX1 U1_23 ( .A(A[23]), .B(B[23]), .C(carry[23]), .YC(carry[24]), .YS(
        SUM[23]) );
  FAX1 U1_22 ( .A(A[22]), .B(B[22]), .C(carry[22]), .YC(carry[23]), .YS(
        SUM[22]) );
  FAX1 U1_21 ( .A(A[21]), .B(B[21]), .C(carry[21]), .YC(carry[22]), .YS(
        SUM[21]) );
  FAX1 U1_20 ( .A(A[20]), .B(B[20]), .C(carry[20]), .YC(carry[21]), .YS(
        SUM[20]) );
  FAX1 U1_19 ( .A(A[19]), .B(B[19]), .C(carry[19]), .YC(carry[20]), .YS(
        SUM[19]) );
  FAX1 U1_18 ( .A(A[18]), .B(B[18]), .C(carry[18]), .YC(carry[19]), .YS(
        SUM[18]) );
  FAX1 U1_17 ( .A(A[17]), .B(B[17]), .C(carry[17]), .YC(carry[18]), .YS(
        SUM[17]) );
  FAX1 U1_16 ( .A(A[16]), .B(B[16]), .C(carry[16]), .YC(carry[17]), .YS(
        SUM[16]) );
  FAX1 U1_15 ( .A(A[15]), .B(B[15]), .C(carry[15]), .YC(carry[16]), .YS(
        SUM[15]) );
  FAX1 U1_14 ( .A(A[14]), .B(B[14]), .C(carry[14]), .YC(carry[15]), .YS(
        SUM[14]) );
  FAX1 U1_13 ( .A(A[13]), .B(B[13]), .C(carry[13]), .YC(carry[14]), .YS(
        SUM[13]) );
  FAX1 U1_12 ( .A(A[12]), .B(B[12]), .C(carry[12]), .YC(carry[13]), .YS(
        SUM[12]) );
  FAX1 U1_11 ( .A(A[11]), .B(B[11]), .C(carry[11]), .YC(carry[12]), .YS(
        SUM[11]) );
  FAX1 U1_10 ( .A(A[10]), .B(B[10]), .C(carry[10]), .YC(carry[11]), .YS(
        SUM[10]) );
  FAX1 U1_9 ( .A(A[9]), .B(B[9]), .C(carry[9]), .YC(carry[10]), .YS(SUM[9]) );
  FAX1 U1_8 ( .A(A[8]), .B(B[8]), .C(carry[8]), .YC(carry[9]), .YS(SUM[8]) );
  FAX1 U1_7 ( .A(A[7]), .B(B[7]), .C(carry[7]), .YC(carry[8]), .YS(SUM[7]) );
  FAX1 U1_6 ( .A(A[6]), .B(B[6]), .C(carry[6]), .YC(carry[7]), .YS(SUM[6]) );
  FAX1 U1_5 ( .A(A[5]), .B(B[5]), .C(carry[5]), .YC(carry[6]), .YS(SUM[5]) );
  FAX1 U1_4 ( .A(A[4]), .B(B[4]), .C(carry[4]), .YC(carry[5]), .YS(SUM[4]) );
  FAX1 U1_3 ( .A(A[3]), .B(B[3]), .C(n1), .YC(carry[4]), .YS(SUM[3]) );
  AND2X1 U1 ( .A(B[2]), .B(A[2]), .Y(n1) );
  XOR2X1 U2 ( .A(B[2]), .B(A[2]), .Y(SUM[2]) );
endmodule


module fir ( clk, in, out );
  input [31:0] in;
  output [63:0] out;
  input clk;
  wire   mc1_9_, mc1_8_, mc1_7_, mc1_6_, mc1_63_, mc1_62_, mc1_61_, mc1_60_,
         mc1_5_, mc1_59_, mc1_58_, mc1_57_, mc1_56_, mc1_55_, mc1_54_, mc1_53_,
         mc1_52_, mc1_51_, mc1_50_, mc1_4_, mc1_49_, mc1_48_, mc1_47_, mc1_46_,
         mc1_45_, mc1_44_, mc1_43_, mc1_42_, mc1_41_, mc1_40_, mc1_3_, mc1_39_,
         mc1_38_, mc1_37_, mc1_36_, mc1_35_, mc1_34_, mc1_33_, mc1_32_,
         mc1_31_, mc1_30_, mc1_2_, mc1_29_, mc1_28_, mc1_27_, mc1_26_, mc1_25_,
         mc1_24_, mc1_23_, mc1_22_, mc1_21_, mc1_20_, mc1_1_, mc1_19_, mc1_18_,
         mc1_17_, mc1_16_, mc1_15_, mc1_14_, mc1_13_, mc1_12_, mc1_11_,
         mc1_10_, mc1_0_, mc0_9_, mc0_8_, mc0_7_, mc0_6_, mc0_63_, mc0_62_,
         mc0_61_, mc0_60_, mc0_5_, mc0_59_, mc0_58_, mc0_57_, mc0_56_, mc0_55_,
         mc0_54_, mc0_53_, mc0_52_, mc0_51_, mc0_50_, mc0_4_, mc0_49_, mc0_48_,
         mc0_47_, mc0_46_, mc0_45_, mc0_44_, mc0_43_, mc0_42_, mc0_41_,
         mc0_40_, mc0_3_, mc0_39_, mc0_38_, mc0_37_, mc0_36_, mc0_35_, mc0_34_,
         mc0_33_, mc0_32_, mc0_31_, mc0_30_, mc0_2_, mc0_29_, mc0_28_, mc0_27_,
         mc0_26_, mc0_25_, mc0_24_, mc0_23_, mc0_22_, mc0_21_, mc0_20_, mc0_1_,
         mc0_19_, mc0_18_, mc0_17_, mc0_16_, mc0_15_, mc0_14_, mc0_13_,
         mc0_12_, mc0_11_, mc0_10_, mc0_0_, mc2_63, mc2_9_, mc2_8_, mc2_7_,
         mc2_6_, mc2_5_, mc2_4_, mc2_3_, mc2_33_, mc2_32_, mc2_31_, mc2_30_,
         mc2_2_, mc2_29_, mc2_28_, mc2_27_, mc2_26_, mc2_25_, mc2_24_, mc2_23_,
         mc2_22_, mc2_21_, mc2_20_, mc2_1_, mc2_19_, mc2_18_, mc2_17_, mc2_16_,
         mc2_15_, mc2_14_, mc2_13_, mc2_12_, mc2_11_, mc2_10_, mc2_0_, n1, n2;
  wire   [63:0] tmpout1;
  wire   [63:0] sum1;
  wire   [63:0] sum2;
  wire   [63:0] sum3;

  DFFPOSX1 tmpout1_reg_63_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[63]) );
  DFFPOSX1 tmpout1_reg_62_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[62]) );
  DFFPOSX1 tmpout1_reg_61_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[61]) );
  DFFPOSX1 tmpout1_reg_60_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[60]) );
  DFFPOSX1 tmpout1_reg_59_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[59]) );
  DFFPOSX1 tmpout1_reg_58_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[58]) );
  DFFPOSX1 tmpout1_reg_57_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[57]) );
  DFFPOSX1 tmpout1_reg_56_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[56]) );
  DFFPOSX1 tmpout1_reg_55_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[55]) );
  DFFPOSX1 tmpout1_reg_54_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[54]) );
  DFFPOSX1 tmpout1_reg_53_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[53]) );
  DFFPOSX1 tmpout1_reg_52_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[52]) );
  DFFPOSX1 tmpout1_reg_51_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[51]) );
  DFFPOSX1 tmpout1_reg_50_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[50]) );
  DFFPOSX1 tmpout1_reg_49_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[49]) );
  DFFPOSX1 tmpout1_reg_48_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[48]) );
  DFFPOSX1 tmpout1_reg_47_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[47]) );
  DFFPOSX1 tmpout1_reg_46_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[46]) );
  DFFPOSX1 tmpout1_reg_45_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[45]) );
  DFFPOSX1 tmpout1_reg_44_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[44]) );
  DFFPOSX1 tmpout1_reg_43_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[43]) );
  DFFPOSX1 tmpout1_reg_42_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[42]) );
  DFFPOSX1 tmpout1_reg_41_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[41]) );
  DFFPOSX1 tmpout1_reg_40_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[40]) );
  DFFPOSX1 tmpout1_reg_39_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[39]) );
  DFFPOSX1 tmpout1_reg_38_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[38]) );
  DFFPOSX1 tmpout1_reg_37_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[37]) );
  DFFPOSX1 tmpout1_reg_36_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[36]) );
  DFFPOSX1 tmpout1_reg_35_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[35]) );
  DFFPOSX1 tmpout1_reg_34_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[34]) );
  DFFPOSX1 tmpout1_reg_33_ ( .D(in[31]), .CLK(clk), .Q(tmpout1[33]) );
  DFFPOSX1 tmpout1_reg_32_ ( .D(in[30]), .CLK(clk), .Q(tmpout1[32]) );
  DFFPOSX1 tmpout1_reg_31_ ( .D(in[29]), .CLK(clk), .Q(tmpout1[31]) );
  DFFPOSX1 tmpout1_reg_30_ ( .D(in[28]), .CLK(clk), .Q(tmpout1[30]) );
  DFFPOSX1 tmpout1_reg_29_ ( .D(in[27]), .CLK(clk), .Q(tmpout1[29]) );
  DFFPOSX1 tmpout1_reg_28_ ( .D(in[26]), .CLK(clk), .Q(tmpout1[28]) );
  DFFPOSX1 tmpout1_reg_27_ ( .D(in[25]), .CLK(clk), .Q(tmpout1[27]) );
  DFFPOSX1 tmpout1_reg_26_ ( .D(in[24]), .CLK(clk), .Q(tmpout1[26]) );
  DFFPOSX1 tmpout1_reg_25_ ( .D(in[23]), .CLK(clk), .Q(tmpout1[25]) );
  DFFPOSX1 tmpout1_reg_24_ ( .D(in[22]), .CLK(clk), .Q(tmpout1[24]) );
  DFFPOSX1 tmpout1_reg_23_ ( .D(in[21]), .CLK(clk), .Q(tmpout1[23]) );
  DFFPOSX1 tmpout1_reg_22_ ( .D(in[20]), .CLK(clk), .Q(tmpout1[22]) );
  DFFPOSX1 tmpout1_reg_21_ ( .D(in[19]), .CLK(clk), .Q(tmpout1[21]) );
  DFFPOSX1 tmpout1_reg_20_ ( .D(in[18]), .CLK(clk), .Q(tmpout1[20]) );
  DFFPOSX1 tmpout1_reg_19_ ( .D(in[17]), .CLK(clk), .Q(tmpout1[19]) );
  DFFPOSX1 tmpout1_reg_18_ ( .D(in[16]), .CLK(clk), .Q(tmpout1[18]) );
  DFFPOSX1 tmpout1_reg_17_ ( .D(in[15]), .CLK(clk), .Q(tmpout1[17]) );
  DFFPOSX1 tmpout1_reg_16_ ( .D(in[14]), .CLK(clk), .Q(tmpout1[16]) );
  DFFPOSX1 tmpout1_reg_15_ ( .D(in[13]), .CLK(clk), .Q(tmpout1[15]) );
  DFFPOSX1 tmpout1_reg_14_ ( .D(in[12]), .CLK(clk), .Q(tmpout1[14]) );
  DFFPOSX1 tmpout1_reg_13_ ( .D(in[11]), .CLK(clk), .Q(tmpout1[13]) );
  DFFPOSX1 tmpout1_reg_12_ ( .D(in[10]), .CLK(clk), .Q(tmpout1[12]) );
  DFFPOSX1 tmpout1_reg_11_ ( .D(in[9]), .CLK(clk), .Q(tmpout1[11]) );
  DFFPOSX1 tmpout1_reg_10_ ( .D(in[8]), .CLK(clk), .Q(tmpout1[10]) );
  DFFPOSX1 tmpout1_reg_9_ ( .D(in[7]), .CLK(clk), .Q(tmpout1[9]) );
  DFFPOSX1 tmpout1_reg_8_ ( .D(in[6]), .CLK(clk), .Q(tmpout1[8]) );
  DFFPOSX1 tmpout1_reg_7_ ( .D(in[5]), .CLK(clk), .Q(tmpout1[7]) );
  DFFPOSX1 tmpout1_reg_6_ ( .D(in[4]), .CLK(clk), .Q(tmpout1[6]) );
  DFFPOSX1 tmpout1_reg_5_ ( .D(in[3]), .CLK(clk), .Q(tmpout1[5]) );
  DFFPOSX1 tmpout1_reg_4_ ( .D(in[2]), .CLK(clk), .Q(tmpout1[4]) );
  DFFPOSX1 tmpout1_reg_3_ ( .D(in[1]), .CLK(clk), .Q(tmpout1[3]) );
  DFFPOSX1 tmpout1_reg_2_ ( .D(in[0]), .CLK(clk), .Q(tmpout1[2]) );
  DFFPOSX1 tmpout2_reg_63_ ( .D(sum1[63]), .CLK(clk), .Q(mc1_63_) );
  DFFPOSX1 tmpout2_reg_62_ ( .D(sum1[62]), .CLK(clk), .Q(mc1_62_) );
  DFFPOSX1 tmpout2_reg_61_ ( .D(sum1[61]), .CLK(clk), .Q(mc1_61_) );
  DFFPOSX1 tmpout2_reg_60_ ( .D(sum1[60]), .CLK(clk), .Q(mc1_60_) );
  DFFPOSX1 tmpout2_reg_59_ ( .D(sum1[59]), .CLK(clk), .Q(mc1_59_) );
  DFFPOSX1 tmpout2_reg_58_ ( .D(sum1[58]), .CLK(clk), .Q(mc1_58_) );
  DFFPOSX1 tmpout2_reg_57_ ( .D(sum1[57]), .CLK(clk), .Q(mc1_57_) );
  DFFPOSX1 tmpout2_reg_56_ ( .D(sum1[56]), .CLK(clk), .Q(mc1_56_) );
  DFFPOSX1 tmpout2_reg_55_ ( .D(sum1[55]), .CLK(clk), .Q(mc1_55_) );
  DFFPOSX1 tmpout2_reg_54_ ( .D(sum1[54]), .CLK(clk), .Q(mc1_54_) );
  DFFPOSX1 tmpout2_reg_53_ ( .D(sum1[53]), .CLK(clk), .Q(mc1_53_) );
  DFFPOSX1 tmpout2_reg_52_ ( .D(sum1[52]), .CLK(clk), .Q(mc1_52_) );
  DFFPOSX1 tmpout2_reg_51_ ( .D(sum1[51]), .CLK(clk), .Q(mc1_51_) );
  DFFPOSX1 tmpout2_reg_50_ ( .D(sum1[50]), .CLK(clk), .Q(mc1_50_) );
  DFFPOSX1 tmpout2_reg_49_ ( .D(sum1[49]), .CLK(clk), .Q(mc1_49_) );
  DFFPOSX1 tmpout2_reg_48_ ( .D(sum1[48]), .CLK(clk), .Q(mc1_48_) );
  DFFPOSX1 tmpout2_reg_47_ ( .D(sum1[47]), .CLK(clk), .Q(mc1_47_) );
  DFFPOSX1 tmpout2_reg_46_ ( .D(sum1[46]), .CLK(clk), .Q(mc1_46_) );
  DFFPOSX1 tmpout2_reg_45_ ( .D(sum1[45]), .CLK(clk), .Q(mc1_45_) );
  DFFPOSX1 tmpout2_reg_44_ ( .D(sum1[44]), .CLK(clk), .Q(mc1_44_) );
  DFFPOSX1 tmpout2_reg_43_ ( .D(sum1[43]), .CLK(clk), .Q(mc1_43_) );
  DFFPOSX1 tmpout2_reg_42_ ( .D(sum1[42]), .CLK(clk), .Q(mc1_42_) );
  DFFPOSX1 tmpout2_reg_41_ ( .D(sum1[41]), .CLK(clk), .Q(mc1_41_) );
  DFFPOSX1 tmpout2_reg_40_ ( .D(sum1[40]), .CLK(clk), .Q(mc1_40_) );
  DFFPOSX1 tmpout2_reg_39_ ( .D(sum1[39]), .CLK(clk), .Q(mc1_39_) );
  DFFPOSX1 tmpout2_reg_38_ ( .D(sum1[38]), .CLK(clk), .Q(mc1_38_) );
  DFFPOSX1 tmpout2_reg_37_ ( .D(sum1[37]), .CLK(clk), .Q(mc1_37_) );
  DFFPOSX1 tmpout2_reg_36_ ( .D(sum1[36]), .CLK(clk), .Q(mc1_36_) );
  DFFPOSX1 tmpout2_reg_35_ ( .D(sum1[35]), .CLK(clk), .Q(mc1_35_) );
  DFFPOSX1 tmpout2_reg_34_ ( .D(sum1[34]), .CLK(clk), .Q(mc1_34_) );
  DFFPOSX1 tmpout2_reg_33_ ( .D(sum1[33]), .CLK(clk), .Q(mc1_33_) );
  DFFPOSX1 tmpout2_reg_32_ ( .D(sum1[32]), .CLK(clk), .Q(mc1_32_) );
  DFFPOSX1 tmpout2_reg_31_ ( .D(sum1[31]), .CLK(clk), .Q(mc1_31_) );
  DFFPOSX1 tmpout2_reg_30_ ( .D(sum1[30]), .CLK(clk), .Q(mc1_30_) );
  DFFPOSX1 tmpout2_reg_29_ ( .D(sum1[29]), .CLK(clk), .Q(mc1_29_) );
  DFFPOSX1 tmpout2_reg_28_ ( .D(sum1[28]), .CLK(clk), .Q(mc1_28_) );
  DFFPOSX1 tmpout2_reg_27_ ( .D(sum1[27]), .CLK(clk), .Q(mc1_27_) );
  DFFPOSX1 tmpout2_reg_26_ ( .D(sum1[26]), .CLK(clk), .Q(mc1_26_) );
  DFFPOSX1 tmpout2_reg_25_ ( .D(sum1[25]), .CLK(clk), .Q(mc1_25_) );
  DFFPOSX1 tmpout2_reg_24_ ( .D(sum1[24]), .CLK(clk), .Q(mc1_24_) );
  DFFPOSX1 tmpout2_reg_23_ ( .D(sum1[23]), .CLK(clk), .Q(mc1_23_) );
  DFFPOSX1 tmpout2_reg_22_ ( .D(sum1[22]), .CLK(clk), .Q(mc1_22_) );
  DFFPOSX1 tmpout2_reg_21_ ( .D(sum1[21]), .CLK(clk), .Q(mc1_21_) );
  DFFPOSX1 tmpout2_reg_20_ ( .D(sum1[20]), .CLK(clk), .Q(mc1_20_) );
  DFFPOSX1 tmpout2_reg_19_ ( .D(sum1[19]), .CLK(clk), .Q(mc1_19_) );
  DFFPOSX1 tmpout2_reg_18_ ( .D(sum1[18]), .CLK(clk), .Q(mc1_18_) );
  DFFPOSX1 tmpout2_reg_17_ ( .D(sum1[17]), .CLK(clk), .Q(mc1_17_) );
  DFFPOSX1 tmpout2_reg_16_ ( .D(sum1[16]), .CLK(clk), .Q(mc1_16_) );
  DFFPOSX1 tmpout2_reg_15_ ( .D(sum1[15]), .CLK(clk), .Q(mc1_15_) );
  DFFPOSX1 tmpout2_reg_14_ ( .D(sum1[14]), .CLK(clk), .Q(mc1_14_) );
  DFFPOSX1 tmpout2_reg_13_ ( .D(sum1[13]), .CLK(clk), .Q(mc1_13_) );
  DFFPOSX1 tmpout2_reg_12_ ( .D(sum1[12]), .CLK(clk), .Q(mc1_12_) );
  DFFPOSX1 tmpout2_reg_11_ ( .D(sum1[11]), .CLK(clk), .Q(mc1_11_) );
  DFFPOSX1 tmpout2_reg_10_ ( .D(sum1[10]), .CLK(clk), .Q(mc1_10_) );
  DFFPOSX1 tmpout2_reg_9_ ( .D(sum1[9]), .CLK(clk), .Q(mc1_9_) );
  DFFPOSX1 tmpout2_reg_8_ ( .D(sum1[8]), .CLK(clk), .Q(mc1_8_) );
  DFFPOSX1 tmpout2_reg_7_ ( .D(sum1[7]), .CLK(clk), .Q(mc1_7_) );
  DFFPOSX1 tmpout2_reg_6_ ( .D(sum1[6]), .CLK(clk), .Q(mc1_6_) );
  DFFPOSX1 tmpout2_reg_5_ ( .D(sum1[5]), .CLK(clk), .Q(mc1_5_) );
  DFFPOSX1 tmpout2_reg_4_ ( .D(sum1[4]), .CLK(clk), .Q(mc1_4_) );
  DFFPOSX1 tmpout2_reg_3_ ( .D(sum1[3]), .CLK(clk), .Q(mc1_3_) );
  DFFPOSX1 tmpout2_reg_2_ ( .D(sum1[2]), .CLK(clk), .Q(mc1_2_) );
  DFFPOSX1 tmpout2_reg_1_ ( .D(sum1[1]), .CLK(clk), .Q(mc1_1_) );
  DFFPOSX1 tmpout2_reg_0_ ( .D(sum1[0]), .CLK(clk), .Q(mc1_0_) );
  DFFPOSX1 tmpout3_reg_63_ ( .D(sum2[63]), .CLK(clk), .Q(mc0_63_) );
  DFFPOSX1 tmpout3_reg_62_ ( .D(sum2[62]), .CLK(clk), .Q(mc0_62_) );
  DFFPOSX1 tmpout3_reg_61_ ( .D(sum2[61]), .CLK(clk), .Q(mc0_61_) );
  DFFPOSX1 tmpout3_reg_60_ ( .D(sum2[60]), .CLK(clk), .Q(mc0_60_) );
  DFFPOSX1 tmpout3_reg_59_ ( .D(sum2[59]), .CLK(clk), .Q(mc0_59_) );
  DFFPOSX1 tmpout3_reg_58_ ( .D(sum2[58]), .CLK(clk), .Q(mc0_58_) );
  DFFPOSX1 tmpout3_reg_57_ ( .D(sum2[57]), .CLK(clk), .Q(mc0_57_) );
  DFFPOSX1 tmpout3_reg_56_ ( .D(sum2[56]), .CLK(clk), .Q(mc0_56_) );
  DFFPOSX1 tmpout3_reg_55_ ( .D(sum2[55]), .CLK(clk), .Q(mc0_55_) );
  DFFPOSX1 tmpout3_reg_54_ ( .D(sum2[54]), .CLK(clk), .Q(mc0_54_) );
  DFFPOSX1 tmpout3_reg_53_ ( .D(sum2[53]), .CLK(clk), .Q(mc0_53_) );
  DFFPOSX1 tmpout3_reg_52_ ( .D(sum2[52]), .CLK(clk), .Q(mc0_52_) );
  DFFPOSX1 tmpout3_reg_51_ ( .D(sum2[51]), .CLK(clk), .Q(mc0_51_) );
  DFFPOSX1 tmpout3_reg_50_ ( .D(sum2[50]), .CLK(clk), .Q(mc0_50_) );
  DFFPOSX1 tmpout3_reg_49_ ( .D(sum2[49]), .CLK(clk), .Q(mc0_49_) );
  DFFPOSX1 tmpout3_reg_48_ ( .D(sum2[48]), .CLK(clk), .Q(mc0_48_) );
  DFFPOSX1 tmpout3_reg_47_ ( .D(sum2[47]), .CLK(clk), .Q(mc0_47_) );
  DFFPOSX1 tmpout3_reg_46_ ( .D(sum2[46]), .CLK(clk), .Q(mc0_46_) );
  DFFPOSX1 tmpout3_reg_45_ ( .D(sum2[45]), .CLK(clk), .Q(mc0_45_) );
  DFFPOSX1 tmpout3_reg_44_ ( .D(sum2[44]), .CLK(clk), .Q(mc0_44_) );
  DFFPOSX1 tmpout3_reg_43_ ( .D(sum2[43]), .CLK(clk), .Q(mc0_43_) );
  DFFPOSX1 tmpout3_reg_42_ ( .D(sum2[42]), .CLK(clk), .Q(mc0_42_) );
  DFFPOSX1 tmpout3_reg_41_ ( .D(sum2[41]), .CLK(clk), .Q(mc0_41_) );
  DFFPOSX1 tmpout3_reg_40_ ( .D(sum2[40]), .CLK(clk), .Q(mc0_40_) );
  DFFPOSX1 tmpout3_reg_39_ ( .D(sum2[39]), .CLK(clk), .Q(mc0_39_) );
  DFFPOSX1 tmpout3_reg_38_ ( .D(sum2[38]), .CLK(clk), .Q(mc0_38_) );
  DFFPOSX1 tmpout3_reg_37_ ( .D(sum2[37]), .CLK(clk), .Q(mc0_37_) );
  DFFPOSX1 tmpout3_reg_36_ ( .D(sum2[36]), .CLK(clk), .Q(mc0_36_) );
  DFFPOSX1 tmpout3_reg_35_ ( .D(sum2[35]), .CLK(clk), .Q(mc0_35_) );
  DFFPOSX1 tmpout3_reg_34_ ( .D(sum2[34]), .CLK(clk), .Q(mc0_34_) );
  DFFPOSX1 tmpout3_reg_33_ ( .D(sum2[33]), .CLK(clk), .Q(mc0_33_) );
  DFFPOSX1 tmpout3_reg_32_ ( .D(sum2[32]), .CLK(clk), .Q(mc0_32_) );
  DFFPOSX1 tmpout3_reg_31_ ( .D(sum2[31]), .CLK(clk), .Q(mc0_31_) );
  DFFPOSX1 tmpout3_reg_30_ ( .D(sum2[30]), .CLK(clk), .Q(mc0_30_) );
  DFFPOSX1 tmpout3_reg_29_ ( .D(sum2[29]), .CLK(clk), .Q(mc0_29_) );
  DFFPOSX1 tmpout3_reg_28_ ( .D(sum2[28]), .CLK(clk), .Q(mc0_28_) );
  DFFPOSX1 tmpout3_reg_27_ ( .D(sum2[27]), .CLK(clk), .Q(mc0_27_) );
  DFFPOSX1 tmpout3_reg_26_ ( .D(sum2[26]), .CLK(clk), .Q(mc0_26_) );
  DFFPOSX1 tmpout3_reg_25_ ( .D(sum2[25]), .CLK(clk), .Q(mc0_25_) );
  DFFPOSX1 tmpout3_reg_24_ ( .D(sum2[24]), .CLK(clk), .Q(mc0_24_) );
  DFFPOSX1 tmpout3_reg_23_ ( .D(sum2[23]), .CLK(clk), .Q(mc0_23_) );
  DFFPOSX1 tmpout3_reg_22_ ( .D(sum2[22]), .CLK(clk), .Q(mc0_22_) );
  DFFPOSX1 tmpout3_reg_21_ ( .D(sum2[21]), .CLK(clk), .Q(mc0_21_) );
  DFFPOSX1 tmpout3_reg_20_ ( .D(sum2[20]), .CLK(clk), .Q(mc0_20_) );
  DFFPOSX1 tmpout3_reg_19_ ( .D(sum2[19]), .CLK(clk), .Q(mc0_19_) );
  DFFPOSX1 tmpout3_reg_18_ ( .D(sum2[18]), .CLK(clk), .Q(mc0_18_) );
  DFFPOSX1 tmpout3_reg_17_ ( .D(sum2[17]), .CLK(clk), .Q(mc0_17_) );
  DFFPOSX1 tmpout3_reg_16_ ( .D(sum2[16]), .CLK(clk), .Q(mc0_16_) );
  DFFPOSX1 tmpout3_reg_15_ ( .D(sum2[15]), .CLK(clk), .Q(mc0_15_) );
  DFFPOSX1 tmpout3_reg_14_ ( .D(sum2[14]), .CLK(clk), .Q(mc0_14_) );
  DFFPOSX1 tmpout3_reg_13_ ( .D(sum2[13]), .CLK(clk), .Q(mc0_13_) );
  DFFPOSX1 tmpout3_reg_12_ ( .D(sum2[12]), .CLK(clk), .Q(mc0_12_) );
  DFFPOSX1 tmpout3_reg_11_ ( .D(sum2[11]), .CLK(clk), .Q(mc0_11_) );
  DFFPOSX1 tmpout3_reg_10_ ( .D(sum2[10]), .CLK(clk), .Q(mc0_10_) );
  DFFPOSX1 tmpout3_reg_9_ ( .D(sum2[9]), .CLK(clk), .Q(mc0_9_) );
  DFFPOSX1 tmpout3_reg_8_ ( .D(sum2[8]), .CLK(clk), .Q(mc0_8_) );
  DFFPOSX1 tmpout3_reg_7_ ( .D(sum2[7]), .CLK(clk), .Q(mc0_7_) );
  DFFPOSX1 tmpout3_reg_6_ ( .D(sum2[6]), .CLK(clk), .Q(mc0_6_) );
  DFFPOSX1 tmpout3_reg_5_ ( .D(sum2[5]), .CLK(clk), .Q(mc0_5_) );
  DFFPOSX1 tmpout3_reg_4_ ( .D(sum2[4]), .CLK(clk), .Q(mc0_4_) );
  DFFPOSX1 tmpout3_reg_3_ ( .D(sum2[3]), .CLK(clk), .Q(mc0_3_) );
  DFFPOSX1 tmpout3_reg_2_ ( .D(sum2[2]), .CLK(clk), .Q(mc0_2_) );
  DFFPOSX1 tmpout3_reg_1_ ( .D(sum2[1]), .CLK(clk), .Q(mc0_1_) );
  DFFPOSX1 tmpout3_reg_0_ ( .D(sum2[0]), .CLK(clk), .Q(mc0_0_) );
  DFFPOSX1 out_reg_63_ ( .D(sum3[63]), .CLK(clk), .Q(out[63]) );
  DFFPOSX1 out_reg_62_ ( .D(sum3[62]), .CLK(clk), .Q(out[62]) );
  DFFPOSX1 out_reg_61_ ( .D(sum3[61]), .CLK(clk), .Q(out[61]) );
  DFFPOSX1 out_reg_60_ ( .D(sum3[60]), .CLK(clk), .Q(out[60]) );
  DFFPOSX1 out_reg_59_ ( .D(sum3[59]), .CLK(clk), .Q(out[59]) );
  DFFPOSX1 out_reg_58_ ( .D(sum3[58]), .CLK(clk), .Q(out[58]) );
  DFFPOSX1 out_reg_57_ ( .D(sum3[57]), .CLK(clk), .Q(out[57]) );
  DFFPOSX1 out_reg_56_ ( .D(sum3[56]), .CLK(clk), .Q(out[56]) );
  DFFPOSX1 out_reg_55_ ( .D(sum3[55]), .CLK(clk), .Q(out[55]) );
  DFFPOSX1 out_reg_54_ ( .D(sum3[54]), .CLK(clk), .Q(out[54]) );
  DFFPOSX1 out_reg_53_ ( .D(sum3[53]), .CLK(clk), .Q(out[53]) );
  DFFPOSX1 out_reg_52_ ( .D(sum3[52]), .CLK(clk), .Q(out[52]) );
  DFFPOSX1 out_reg_51_ ( .D(sum3[51]), .CLK(clk), .Q(out[51]) );
  DFFPOSX1 out_reg_50_ ( .D(sum3[50]), .CLK(clk), .Q(out[50]) );
  DFFPOSX1 out_reg_49_ ( .D(sum3[49]), .CLK(clk), .Q(out[49]) );
  DFFPOSX1 out_reg_48_ ( .D(sum3[48]), .CLK(clk), .Q(out[48]) );
  DFFPOSX1 out_reg_47_ ( .D(sum3[47]), .CLK(clk), .Q(out[47]) );
  DFFPOSX1 out_reg_46_ ( .D(sum3[46]), .CLK(clk), .Q(out[46]) );
  DFFPOSX1 out_reg_45_ ( .D(sum3[45]), .CLK(clk), .Q(out[45]) );
  DFFPOSX1 out_reg_44_ ( .D(sum3[44]), .CLK(clk), .Q(out[44]) );
  DFFPOSX1 out_reg_43_ ( .D(sum3[43]), .CLK(clk), .Q(out[43]) );
  DFFPOSX1 out_reg_42_ ( .D(sum3[42]), .CLK(clk), .Q(out[42]) );
  DFFPOSX1 out_reg_41_ ( .D(sum3[41]), .CLK(clk), .Q(out[41]) );
  DFFPOSX1 out_reg_40_ ( .D(sum3[40]), .CLK(clk), .Q(out[40]) );
  DFFPOSX1 out_reg_39_ ( .D(sum3[39]), .CLK(clk), .Q(out[39]) );
  DFFPOSX1 out_reg_38_ ( .D(sum3[38]), .CLK(clk), .Q(out[38]) );
  DFFPOSX1 out_reg_37_ ( .D(sum3[37]), .CLK(clk), .Q(out[37]) );
  DFFPOSX1 out_reg_36_ ( .D(sum3[36]), .CLK(clk), .Q(out[36]) );
  DFFPOSX1 out_reg_35_ ( .D(sum3[35]), .CLK(clk), .Q(out[35]) );
  DFFPOSX1 out_reg_34_ ( .D(sum3[34]), .CLK(clk), .Q(out[34]) );
  DFFPOSX1 out_reg_33_ ( .D(sum3[33]), .CLK(clk), .Q(out[33]) );
  DFFPOSX1 out_reg_32_ ( .D(sum3[32]), .CLK(clk), .Q(out[32]) );
  DFFPOSX1 out_reg_31_ ( .D(sum3[31]), .CLK(clk), .Q(out[31]) );
  DFFPOSX1 out_reg_30_ ( .D(sum3[30]), .CLK(clk), .Q(out[30]) );
  DFFPOSX1 out_reg_29_ ( .D(sum3[29]), .CLK(clk), .Q(out[29]) );
  DFFPOSX1 out_reg_28_ ( .D(sum3[28]), .CLK(clk), .Q(out[28]) );
  DFFPOSX1 out_reg_27_ ( .D(sum3[27]), .CLK(clk), .Q(out[27]) );
  DFFPOSX1 out_reg_26_ ( .D(sum3[26]), .CLK(clk), .Q(out[26]) );
  DFFPOSX1 out_reg_25_ ( .D(sum3[25]), .CLK(clk), .Q(out[25]) );
  DFFPOSX1 out_reg_24_ ( .D(sum3[24]), .CLK(clk), .Q(out[24]) );
  DFFPOSX1 out_reg_23_ ( .D(sum3[23]), .CLK(clk), .Q(out[23]) );
  DFFPOSX1 out_reg_22_ ( .D(sum3[22]), .CLK(clk), .Q(out[22]) );
  DFFPOSX1 out_reg_21_ ( .D(sum3[21]), .CLK(clk), .Q(out[21]) );
  DFFPOSX1 out_reg_20_ ( .D(sum3[20]), .CLK(clk), .Q(out[20]) );
  DFFPOSX1 out_reg_19_ ( .D(sum3[19]), .CLK(clk), .Q(out[19]) );
  DFFPOSX1 out_reg_18_ ( .D(sum3[18]), .CLK(clk), .Q(out[18]) );
  DFFPOSX1 out_reg_17_ ( .D(sum3[17]), .CLK(clk), .Q(out[17]) );
  DFFPOSX1 out_reg_16_ ( .D(sum3[16]), .CLK(clk), .Q(out[16]) );
  DFFPOSX1 out_reg_15_ ( .D(sum3[15]), .CLK(clk), .Q(out[15]) );
  DFFPOSX1 out_reg_14_ ( .D(sum3[14]), .CLK(clk), .Q(out[14]) );
  DFFPOSX1 out_reg_13_ ( .D(sum3[13]), .CLK(clk), .Q(out[13]) );
  DFFPOSX1 out_reg_12_ ( .D(sum3[12]), .CLK(clk), .Q(out[12]) );
  DFFPOSX1 out_reg_11_ ( .D(sum3[11]), .CLK(clk), .Q(out[11]) );
  DFFPOSX1 out_reg_10_ ( .D(sum3[10]), .CLK(clk), .Q(out[10]) );
  DFFPOSX1 out_reg_9_ ( .D(sum3[9]), .CLK(clk), .Q(out[9]) );
  DFFPOSX1 out_reg_8_ ( .D(sum3[8]), .CLK(clk), .Q(out[8]) );
  DFFPOSX1 out_reg_7_ ( .D(sum3[7]), .CLK(clk), .Q(out[7]) );
  DFFPOSX1 out_reg_6_ ( .D(sum3[6]), .CLK(clk), .Q(out[6]) );
  DFFPOSX1 out_reg_5_ ( .D(sum3[5]), .CLK(clk), .Q(out[5]) );
  DFFPOSX1 out_reg_4_ ( .D(sum3[4]), .CLK(clk), .Q(out[4]) );
  DFFPOSX1 out_reg_3_ ( .D(sum3[3]), .CLK(clk), .Q(out[3]) );
  DFFPOSX1 out_reg_2_ ( .D(sum3[2]), .CLK(clk), .Q(out[2]) );
  DFFPOSX1 out_reg_1_ ( .D(sum3[1]), .CLK(clk), .Q(out[1]) );
  DFFPOSX1 out_reg_0_ ( .D(sum3[0]), .CLK(clk), .Q(out[0]) );
  fir_DW01_sub_1 sub_0_root_add_0_root_add_17 ( .A({mc1_63_, mc1_62_, mc1_61_, 
        mc1_60_, mc1_59_, mc1_58_, mc1_57_, mc1_56_, mc1_55_, mc1_54_, mc1_53_, 
        mc1_52_, mc1_51_, mc1_50_, mc1_49_, mc1_48_, mc1_47_, mc1_46_, mc1_45_, 
        mc1_44_, mc1_43_, mc1_42_, mc1_41_, mc1_40_, mc1_39_, mc1_38_, mc1_37_, 
        mc1_36_, mc1_35_, mc1_34_, mc1_33_, mc1_32_, mc1_31_, mc1_30_, mc1_29_, 
        mc1_28_, mc1_27_, mc1_26_, mc1_25_, mc1_24_, mc1_23_, mc1_22_, mc1_21_, 
        mc1_20_, mc1_19_, mc1_18_, mc1_17_, mc1_16_, mc1_15_, mc1_14_, mc1_13_, 
        mc1_12_, mc1_11_, mc1_10_, mc1_9_, mc1_8_, mc1_7_, mc1_6_, mc1_5_, 
        mc1_4_, mc1_3_, mc1_2_, mc1_1_, mc1_0_}), .B({in[31], in[31], in[31], 
        in[31], in[31], in[31], in[31], in[31], in[31], in[31], in[31], in[31], 
        in[31], in[31], in[31], in[31], in[31], in[31], in[31], in[31], in[31], 
        in[31], in[31], in[31], in[31], in[31], in[31], in[31], in[31], in[31], 
        in[31], in[31], in}), .CI(1'b0), .DIFF(sum2), .CO() );
  fir_DW01_sub_0 sub_0_root_add_0_root_add_18 ( .A({mc0_63_, mc0_62_, mc0_61_, 
        mc0_60_, mc0_59_, mc0_58_, mc0_57_, mc0_56_, mc0_55_, mc0_54_, mc0_53_, 
        mc0_52_, mc0_51_, mc0_50_, mc0_49_, mc0_48_, mc0_47_, mc0_46_, mc0_45_, 
        mc0_44_, mc0_43_, mc0_42_, mc0_41_, mc0_40_, mc0_39_, mc0_38_, mc0_37_, 
        mc0_36_, mc0_35_, mc0_34_, mc0_33_, mc0_32_, mc0_31_, mc0_30_, mc0_29_, 
        mc0_28_, mc0_27_, mc0_26_, mc0_25_, mc0_24_, mc0_23_, mc0_22_, mc0_21_, 
        mc0_20_, mc0_19_, mc0_18_, mc0_17_, mc0_16_, mc0_15_, mc0_14_, mc0_13_, 
        mc0_12_, mc0_11_, mc0_10_, mc0_9_, mc0_8_, mc0_7_, mc0_6_, mc0_5_, 
        mc0_4_, mc0_3_, mc0_2_, mc0_1_, mc0_0_}), .B({in[31], in[31], in[31], 
        in[31], in[31], in[31], in[31], in[31], in[31], in[31], in[31], in[31], 
        in[31], in[31], in[31], in[31], in[31], in[31], in[31], in[31], in[31], 
        in[31], in[31], in[31], in[31], in[31], in[31], in[31], in[31], in[31], 
        in[31], in, 1'b0}), .CI(1'b0), .DIFF(sum3), .CO() );
  fir_DW_mult_tc_0 mult_13 ( .a({1'b0, 1'b1, 1'b1}), .b(in), .product({mc2_63, 
        mc2_33_, mc2_32_, mc2_31_, mc2_30_, mc2_29_, mc2_28_, mc2_27_, mc2_26_, 
        mc2_25_, mc2_24_, mc2_23_, mc2_22_, mc2_21_, mc2_20_, mc2_19_, mc2_18_, 
        mc2_17_, mc2_16_, mc2_15_, mc2_14_, mc2_13_, mc2_12_, mc2_11_, mc2_10_, 
        mc2_9_, mc2_8_, mc2_7_, mc2_6_, mc2_5_, mc2_4_, mc2_3_, mc2_2_, mc2_1_, 
        mc2_0_}) );
  fir_DW01_add_2 add_16 ( .A({tmpout1[63:2], 1'b0, 1'b0}), .B({mc2_33_, 
        mc2_33_, mc2_33_, mc2_33_, mc2_33_, mc2_33_, mc2_33_, mc2_33_, mc2_33_, 
        mc2_33_, mc2_33_, n1, n1, n1, n1, n1, n1, n1, n1, n1, n1, n1, n1, n1, 
        n1, n1, mc2_33_, mc2_33_, mc2_33_, mc2_33_, mc2_33_, mc2_32_, mc2_31_, 
        mc2_30_, mc2_29_, mc2_28_, mc2_27_, mc2_26_, mc2_25_, mc2_24_, mc2_23_, 
        mc2_22_, mc2_21_, mc2_20_, mc2_19_, mc2_18_, mc2_17_, mc2_16_, mc2_15_, 
        mc2_14_, mc2_13_, mc2_12_, mc2_11_, mc2_10_, mc2_9_, mc2_8_, mc2_7_, 
        mc2_6_, mc2_5_, mc2_4_, mc2_3_, mc2_2_, mc2_1_, mc2_0_}), .CI(1'b0), 
        .SUM(sum1), .CO() );
  INVX1 U5 ( .A(n2), .Y(n1) );
  INVX1 U6 ( .A(mc2_63), .Y(n2) );
endmodule

