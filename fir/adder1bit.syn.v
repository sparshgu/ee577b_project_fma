
module halfadder ( a, b, sum, carry );
  input a, b;
  output sum, carry;


  XOR2X1 U2 ( .A(b), .B(a), .Y(sum) );
  AND2X1 U3 ( .A(a), .B(b), .Y(carry) );
endmodule

