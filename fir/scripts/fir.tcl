######################################################################
#
# EE-577b 2015  Spring
# : DesignCompiler synthesis script for 1-bit adder
#   modified by Aditya Deshpande
#
# use this script as a template for synthesizing combinational logic
#
######################################################################

# Setting variable for design_name. (top module name)
set design_name fir;

# Reading source verilog file.
# copy your verilog file into ./src/ before synthesis.
read_verilog ./src/fir.v;

# Setting $design_name as current working design.
# Use this command before setting any constraints.
current_design $design_name ;

# If you have multiple instances of the same module,
# use this so that DesignCompiler optimizes each instance separately.
uniquify ;

# Linking your design into the cells in standard cell libraries.
# This command checks whether your design can be compiled
# with the target libraries specified in the .synopsys_dc.setup file.
link ;

create_clock clk -period 10 -waveform {0 5};
set_clock_latency 0.3 clk;

# Setting timing constraints for combinational logic.
# Specifying maximum delay from inputs to outputs
set_max_delay 5.0 -from [all_inputs] -to [all_outputs];

set_input_delay 2.5 -clock clk [all_inputs]

set_output_delay 2.5 -clock clk [all_outputs]

# "check_design" checks the internal representation of the
# current design for consistency and issues error and
# warning messages as appropriate.
check_design > reports/$design_name.check_design ;

# Perforing synthesis and optimization on the current_design.
compile ;

# For better synthesis result, use "compile_ultra" command.
# compile_ultra is doing automatic ungrouping during optimization,
# therefore sometimes it's hard to figure out the critical path
# from the synthesized netlist.


# Writing the synthesis result into Synopsys db format.
# You can read the saved db file into DesignCompiler later using
# "read_db" command for further analysis (timing, area...).
#write -xg_force_db -format db -hierarchy -out db/$design_name.db ;

# Generating timing and are report of the synthezied design.
report_timing > reports/$design_name.timing ;
report_area > reports/$design_name.area ;

# Writing synthesized gate-level verilog netlist.
# This verilog netlist will be used for post-synthesis gate-level simulation.
change_names -rules verilog -hierarchy ;
write -format verilog -hierarchy -out netlist/$design_name.syn.v ;

# Writing Standard Delay Format (SDF) back-annotation file.
# This delay information can be used for post-synthesis simulation.
write_sdf sdf/$design_name.sdf;
write_sdc sdc/$design_name.sdc;