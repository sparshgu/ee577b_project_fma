#! /bin/csh -f

rm -rf work/ 
mkdir work

# analyze design and testbench  (top_module should be the last file analyzed)
ncvlog -messages -work work ./design/adder1bit.v
ncvlog -messages -work work ./tb/tb.v

# elaborates and creates snapshot of design (top_module)
ncelab -messages -work work -libverbose -timescale 1ns/1ps -access +r work.tb

# Run the design (top_module)
ncsim work.tb
#ncsim -gui work.tb_mult_sim

