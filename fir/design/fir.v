module fir(
        input clk,
        input signed [31:0] in,
        output reg signed [63:0] out
        );
//filter coefficient fc = [-2 -1 3 4]	
wire signed   [31:0] fc0=-2,fc1=-1,fc2=3,fc3=4;  
 
wire signed   [63:0] mc0,mc1,mc2,mc3,sum1,sum2,sum3;
reg signed [63:0] tmpout1,tmpout2,tmpout3;
    
    assign mc3 = fc3*in;
    assign mc2 = fc2*in;
    assign mc1 = fc1*in;
    assign mc0 = fc0*in;
    assign sum1 = tmpout1 + mc2;
    assign sum2 = tmpout2 + mc1;
    assign sum3 = tmpout3 + mc0;    

always@ (posedge clk)
begin
    tmpout1<=mc3;
	tmpout2<=sum1;
	tmpout3<=sum2;
	out<=sum3;
end		
	

endmodule