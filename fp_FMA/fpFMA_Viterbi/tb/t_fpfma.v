// Code your testbench here
// or browse Examples
`timescale 1ns/10ps
`define NULL 0
`define EOF -1
`include "/home/scf-22/ee577/NCSU45PDK/FreePDK45/osu_soc/lib/files/gscl45nm.v"
`include "/home/scf-22/ee577/design_pdk/osu_stdcells/lib/tsmc018/lib/osu018_stdcells.v"
module t_fpfma();
  parameter WIDTH=32;
  parameter CLOCK_PERIOD = 5;

  reg clk;
  reg reset;
  reg [WIDTH-1:0] A,B,C,x_ideal;
  wire[WIDTH-1:0] result;
  reg [1:0] round;

   initial begin
  	$sdf_annotate("sdf/fpfma.sdf",UUT,,,"TYPICAL", "1.0:1.0:1.0", "FROM_MTM");
   end  
  
   fpfma UUT(A, B, C, round, clk, reset,result);
  
   always #(CLOCK_PERIOD/2.0) clk = ~clk;
  
  integer fd;
  
  initial clk = 1'b0;

  initial begin
    $dumpfile("dump.vcd"); $dumpvars;
	reset = 1;
	#(3*CLOCK_PERIOD);
	reset = 0;
	#(3*CLOCK_PERIOD);
	round=2'b01;
	fd=$fopen("testData.txt","r");
	if (fd == `NULL) begin
        	$display("fileR handle was NULL");
        	$finish;
	end
	while (! $feof(fd)) begin
      @(posedge clk) 
			$fscanf(fd, "%x %x %x %x", A, B, C, x_ideal);
      #(CLOCK_PERIOD - 1);
       $display("A = %x * B = %x + C = %x => Result = %x Ideal = %x",A,B,C,result,x_ideal);
	end
	$fclose(fd);
	$finish;
  end
endmodule
