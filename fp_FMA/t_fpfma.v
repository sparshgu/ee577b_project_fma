`timescale 1ns/10ps
`define NULL 0
`define EOF -1
module t_fpfma();
  parameter WIDTH=32;
  parameter CLOCK_PERIOD = 5;

  reg clk;
  reg reset;
  reg [WIDTH-1:0] A,B,C,x_ideal;
  reg [1:0] round;  
  
  fpfma UUT(A, B, C, round, clk, reset,result);
  
  initial clk = 1'b0;

  always #(CLOCK_PERIOD/2.0) clk = ~clk;
  
  integer fd;
  

  initial begin
	reset = 1;
	#(3*CLOCK_PERIOD);
	reset = 0;
	#(3*CLOCK_PERIOD);
	round=2'b01;
	fd=$fopen("sub_inputs.txt", "r");
	if (fd == `NULL) begin
        	$display("fileR handle was NULL");
        	$finish;
        end
  end
  always @(posedge clk) begin
        $fscanf(fd, "%x %x %x %x", A, B, C, x_ideal);
	if (!$feof(fd)) begin
	  //$display("A = %x * B = %x + C = %x => Result = %x Ideal = %x",A,B,C,result,x_ideal);
  	end else begin
		$fclose(fd);
   		$finish;
   	end
  end
initial begin
 	$monitor("A = %x * B = %x + C = %x => Result = %x Ideal = %x",A,B,C,result,x_ideal);
end
endmodule
